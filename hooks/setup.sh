#!/bin/bash

# setup.sh
# Sets up the current repository with utility scripts such as a "pre-commit" git
# hook that (among other things) checks ot see if all the modified C files in
# the current commit are formatted.
#
# I tried to make this script idempotent, so running this command will have
# repeatable results. Please let me know if there's a bug here.
#
# Ideally, people just do `source setup.sh` and:
# 1. Have the git hooks set up properly
# 2. Have access to utility functions such as "build" or "get_modified_c_files"

ROOT_DIR=$(git rev-parse --show-toplevel)

# shellcheck source=./util.sh
source "$ROOT_DIR/hooks/util.sh"

git_hooks_dir="$ROOT_DIR/.git/hooks/"
backup_hooks_dir="$ROOT_DIR/.git/hooks-backup/"

if [[ -d $backup_hooks_dir ]]; then
    if ! rm -rf "$backup_hooks_dir" ; then
        echo "Removing backup dir failed."
        exit 1
    fi
fi

# Backup the current /.git/hooks/ directory
cp -r "$git_hooks_dir" "$backup_hooks_dir"

# Remove the current hooks directory / symbolic link, if it exists
# Note that we check for symlinks specifically first because if we don't we'll
# remove the entire $ROOT_DIR/hooks directory (the current one).
git_hooks_link="$ROOT_DIR/.git/hooks"
if [[ -L $git_hooks_link ]]; then
    echo "Removing link from .git/hooks to hooks/"
    rm -v "$git_hooks_link"
fi


if [[ -d "$git_hooks_dir" ]]; then
    echo "Removing base .git/hooks directory"
    rm -rfv "$git_hooks_dir"
fi

# Now, have the git hooks directory be the current directory.
new_hooks_dir="$ROOT_DIR/hooks"

echo "$new_hooks_dir" "$git_hooks_link"
ln -v --symbolic "$new_hooks_dir" "$git_hooks_link"

chmod +x "$ROOT_DIR/hooks/pre-commit"

# Setup code completion file by creating a soft link to the
# build/compile-commands.json file (useful for emacs)
BUILD_DIR="$ROOT_DIR/build"
if [[ -d "$BUILD_DIR" ]]; then
    mkdir "$BUILD_DIR"
fi
touch "$BUILD_DIR/compile_commands.json"
ln -s "$BUILD_DIR/compile_commands.json" "$ROOT_DIR/compile_commands.json"
