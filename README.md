# Requirements
meson >=0.57.0 (lower this if confirmed working with older versions)
ninja >= 1.10.2
protobuf-compiler

# Building
```
$ meson build
$ ninja -C build
```

# Running
```
$ ./build/mmorpg
```
