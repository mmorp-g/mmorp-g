#ifndef MMO_ITEM_H
#define MMO_ITEM_H


/// libraries ///////////////////////////////////////
#include "Effects.h"

/// forward declarations ///////////////////////////

namespace MMO{

    /// forward declarations //////////////////////

    /// type declarations //////////////////////////
    enum ItemType {
        ITEM_TYPE_EQUIPPABLE,
        ITEM_TYPE_EQUIPPABLE_PROCS,         //equips with on proc effects
        ITEM_TYPE_ON_USE,
    };


    struct Item {
        uint32_t id;
        uint32_t tex;                       //TODO: add texture enums

        ItemType itemType;
        Effect *effects;
    };

}   // namespace MMO

#endif  //MMO_ITEM_H