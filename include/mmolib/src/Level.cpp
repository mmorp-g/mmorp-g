#include "../Level.h"

namespace MMO {

    Level::Level()
    {
    }

    Level::~Level()
    {
    }

    void Level::setMesh(Mesh mesh)
    {
        levelMesh = mesh;
        levelModel = LoadModelFromMesh(levelMesh);
    }

    const Mesh& Level::getMesh() const
    {
    return levelMesh;
    }

    const Model& Level::getModel() const
    {
        return levelModel;
    }

    void Level::updateLevelMesh()
    {
        UpdateMeshBuffer(levelMesh, 0, levelMesh.vertices, (levelMesh.vertexCount * 3) * sizeof(float), 0);
    }

    void Level::addTextureToMap(Texture texture, int mapType)
    {
        textureMap.push_back(texture);
        SetMaterialTexture(levelModel.materials, mapType, texture);
    }

    const std::vector<Texture>& Level::getTextureMap() const
    {
        return textureMap;
    }

	std::vector<int> Level::getVerticesWithinRange(Vector3 pos, float range)
	{

		std::vector<int> verts{};
		for(int i = 0; i < levelMesh.vertexCount * 3; i++)
		{
			float dx = levelMesh.vertices[(i * 3) + 0] - pos.x;
			float dy = levelMesh.vertices[(i * 3) + 1] - pos.y;
			float dz = levelMesh.vertices[(i * 3) + 2] - pos.z;

			float dist = std::sqrt(dx*dx + dy*dy + dz*dz);

			if(dist <= range)
			{
				verts.push_back((i * 3) + 0);
				verts.push_back((i * 3) + 1);
				verts.push_back((i * 3) + 2);
			}
		}

		return verts;

	}

}   // namespace MMO