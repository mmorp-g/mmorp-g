#ifndef MMO_RESOURCE_MANAGER_H
#define MMO_RESOURCE_MANAGER_H

/// libraries ///////////////////////////////////////
#include "AssetPool.h"

#include <raylib.h>

#include <cstdlib>
#include <cstring>

/// forward declarations ///////////////////////////

namespace MMO{

    /// forward declarations //////////////////////

    /// type declarations //////////////////////////
    static AssetPool<Texture2D> texturePool = AssetPool<Texture2D>(UnloadTexture);
    static AssetPool<Model> modelPool = AssetPool<Model>(UnloadModel);

    /***
     * Hashes string
     * @param fPath String to nHash
     * @return Hash of string
     */
    size_t nHash(size_t n, char *fPath);

    void initResourceManager();

    /***
     * Loads (if necessary) the Texture2D specified in FName (!! CURRENTLY ONLY PNG'S ARE SUPPORTED !!)
     * @param fName File to load
     * @return Loaded Texture2D
     */
    AssetEntry<Texture2D> getTexture(char *fName);

    /***
     * release texture
     * @param texture Texture to release
     */
    void releaseTexture(AssetEntry <Texture2D> texture);

    /***
     * Loads (if necessary) the Model specified in FName (!! CURRENTLY ONLY GLTF IS SUPPORTED !!)
     * @param fName File to load
     * @return Loaded Model
     */
    AssetEntry<Model> getModel(char *fName);

    /***
     * release model
     * @param model Model to release
     */
    void releaseModel(AssetEntry <Model> model);

}   // namespace MMO

#endif  //MMO_RESOURCE_MANAGER_H