#ifndef MMO_ENTITY_H
#define MMO_ENTITY_H

/// libraries ///////////////////////////////////////
#include <cstdint>

/// forward declarations ///////////////////////////

namespace MMO{

    /// forward declarations //////////////////////

    /// type declarations //////////////////////////
    enum AnimationState {
        ANIMATION_STATE_IDLE,
        ANIMATION_STATE_WALKING,
    };

    enum HeadColor {
        HEAD_COLOR_WHITE1,
    };

// enums corresponding to what meshes to load
    enum HeadMesh {
        HEAD_MESH_TEST,
    };

    enum BodyMesh {
        BODY_MESH_TEST,
    };

    enum HandMesh {
        HAND_MESH_TEST,
    };

    enum LegMesh {
        LEG_MESH_TEST,
    };

    enum FootMesh {
        FOOT_MESH_TEST,
    };

    enum WeaponMesh {
        WEAPON_MESH_TEST,
    };

    struct NPCData {
        //TODO: quest and chat interaction
    };

    struct PlayerData {
        //TODO: quest and chat interaction
        //TODO: Skills and inventory tracking
    };

    enum FactionFlags {            //if the hostile flag is not set then entity is friendly
        FACTION_PLAYER_HOSTILE = 1,
        FACTION_NPC_HOSTILE = 2,
        FACTION_ENTITY_HOSTILE = 4,
    };

    enum EntityType {
        ENTITY_TYPE_PLAYER,
        ENTITY_TYPE_NPC,
        ENTITY_TYPE_ENEMY,
    };

    struct Entity {

        uint32_t id;            //0 for unused
        EntityType type;

        float x, y, z;
        float speed;
        float direction;
        AnimationState animationState;

        HeadColor color;
        HeadMesh head;
        BodyMesh body;
        HandMesh hands;
        LegMesh legs;
        FootMesh feet;
        WeaponMesh weapon;

        FactionFlags factionFlags;
        void *typeData;            //different data structure for entity types

    };

}   // namespace MMO;

#endif // MMO_ENTITY_H