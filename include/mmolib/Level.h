#ifndef MMO_LEVEL_H
#define MMO_LEVEL_H

/// libraries ///////////////////////////////////////
#include <raylib.h>

#include <vector>
#include <cmath>

/// forward declarations ///////////////////////////

namespace MMO{

    /// forward declarations //////////////////////

    /// type declarations //////////////////////////
    class Level {

    public:

        Level();

        ~Level();

        /***
         * Sets level mesh
         * @param mesh Level mesh
         */
        void setMesh(Mesh mesh);

        /***
         * Returns Level mesh
         * @return Level mesh
         */
        const Mesh &getMesh() const;

        /***
         * Returns the model wrapper of the level mesh
         * @return Model Wrapper
         */
        const Model &getModel() const;

        /***
         * Update mesh buffers
         */
        void updateLevelMesh();

        /***
         * Adds a texture to the texture map
         * @param texture Texture to add
         * @param mapType Type of texture
         */
        void addTextureToMap(Texture texture, int mapType);

        /***
         * Returns the texture map
         * @return Level texture map
         */
        const std::vector<Texture> &getTextureMap() const;


	 std::vector<int> getVerticesWithinRange(Vector3 pos, float range);


    protected:

        Mesh levelMesh;
        Model levelModel;

        std::vector<Texture> textureMap;

    };

}   // namespace MMO

#endif  //MMO_LEVEL_H