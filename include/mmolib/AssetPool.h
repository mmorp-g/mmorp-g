#ifndef MMO_ASSETPOOL_H
#define MMO_ASSETPOOL_H

/// libraries ///////////////////////////////////////
#include <cstdint>
#include <cstdlib>
#include <map>
#include <utility>

/// forward declarations ///////////////////////////

namespace MMO{

    /// forward declarations //////////////////////
    template<class T>
    struct AssetEntry;

    /// type declarations //////////////////////////
    template<class T>
    class AssetPool
    {
    public:

        AssetPool(void (*deallocCallback)(T));

        /***
         * Inserts asset into the asset pool
         * @param ID Hash of the asset
         * @param t Asset to insert
         * @return
         */
        AssetEntry <T> insert(size_t hash, const T& t);

        /***
         * Removes an asset from the asset pool
         * @param hash Hash of the asset to remove
         */
        void remove(size_t hash);

        /***
         * Returns the asset with the corresponding nHash
         * @param hash Hash of the asset to find
         * @return Asset with corresponding nHash
         */
        AssetEntry<T> find(size_t hash);

        /***
         * Returns true if the asset pool contains an asset with the corresponding nHash
         * @param ID Hash of the asset to find
         * @return True if the asset with corresponding nHash exists
         */
        bool contains(size_t ID);

        /***
         * Returns the current size of the asset pool
         * @return The current size of the asset pool
         */
        size_t size();

        /***
         * Returns the current reference count of the asset with corresponding nHash
         * @param ID Hash of the Asset to count
         * @return the current reference count of the asset with corresponding nHash
         */
        size_t count(size_t ID);

    private:

        using hash = size_t;
        std::map<hash, AssetEntry<T>> assetEntries;

        void (*deallocCallback)(T);
    };

    template<class T>
    struct AssetEntry
    {
        T asset;
        size_t assetCount;
        size_t hash;

        operator T() { return asset; }

        operator const T() const { return asset; }
    };

    template<class T>
    AssetPool<T>::AssetPool(void (*deallocCallback)(T))
    {
        assetEntries = {};
        this->deallocCallback = deallocCallback;
    }

    template<class T>
    void AssetPool<T>::remove(size_t hash)
    {
        AssetEntry<T> t = find(hash);
        if (t.hash != 0 && !--t.assetCount) {
            deallocCallback(t.asset);
            assetEntries.erase(hash);
        }
    }

    template<class T>
    AssetEntry <T> AssetPool<T>::insert(size_t hash, const T& t)
    {
        auto entry = AssetEntry<T>{t, 1, hash};
        assetEntries.emplace(hash, entry);
        return entry;
    }

    template<class T>
    AssetEntry<T> AssetPool<T>::find(size_t hash)
    {
        auto t = assetEntries.find(hash);
        if(t != assetEntries.cend())
            return t->second;
        else
            return AssetEntry<T>{}; // return a blank asset to signal asset not found
    }

    template<class T>
    bool AssetPool<T>::contains(size_t ID)
    {
        return assetEntries.find(ID) != assetEntries.cend();
    }

    template<class T>
    size_t AssetPool<T>::size()
    {
        return assetEntries.size();
    }

    template<class T>
    size_t AssetPool<T>::count(size_t ID)
    {
        size_t count = 0;

        auto it = assetEntries.find(ID);
        if (it != assetEntries.end())
        {
            count = it->second.assetCount;
        }
        else
        {
            count = 0;
        }

        return count;
    }

}   // namespace MMO

#endif  //MMO_ASSETPOOL_H