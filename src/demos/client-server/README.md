# Client-Server Demo

This represents a very dumbed-down version of the client/server flow that we
expect the game to have. 

Below is an explanation of what's happening: 

- The server is spun up
- Once a client is started, it sends the server a request to register a new
player and it receives in response a list of all the players it needs to render. 
- Once a client moves, it sends that move to the server, and the server sends
the updated list of clients to all other clients. 

Currently, updating a position sends N+1 requests, one from the initial client
updating its position, and then N requests from the server to all clients with
the updated list of clients. 
