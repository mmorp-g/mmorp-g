#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <iostream>

#include "basic_character.pb.h"

constexpr size_t BUFFER_LENGTH = 2048;
constexpr size_t UDP_NO_FLAGS = 0;
void SendMessage(struct sockaddr_in* server, int server_fd)
{
  static char message_buffer[BUFFER_LENGTH];

  character::CreateCharacterRequest req;
  req.set_name("A!");

  character::Request to_send;
  to_send.mutable_create_char()->CopyFrom(req);
  to_send.set_type(character::CREATE_CHARACTER);

  size_t message_length = to_send.ByteSizeLong();

  bool ser_result = to_send.SerializeToArray(message_buffer, message_length);
  if(!ser_result)
  {
    perror("Something went wrong in serializing!\n");
    exit(2);
  }

  size_t addrlen = sizeof(*server);
  ssize_t result = sendto(server_fd, message_buffer, message_length, UDP_NO_FLAGS,
                          (struct sockaddr*)server, addrlen);
  if(result < 0)
  {
    printf("Ah!\n");
    fprintf(stderr, "Something went wrong when sending the initial message!\n ERRNO: %d | %s\n", errno, strerror(errno));
    exit(1);
  }
}

void ReceiveMessage(struct sockaddr_in* server, int server_fd)
{
  static char message_buffer[BUFFER_LENGTH];
  socklen_t addrlen = sizeof(*server);
  
  ssize_t bytes_received = recvfrom(server_fd, message_buffer, BUFFER_LENGTH,
                                    UDP_NO_FLAGS,
                                    (struct sockaddr*) server, &addrlen);

  if(bytes_received < 0)
  {
    fprintf(stderr, "Error in receiving: %d", errno);
    exit(2);
  }

  character::Response resp;
  bool success = resp.ParseFromArray(message_buffer, bytes_received);
  if(!success)
  {
    fprintf(stderr, "Error in decoding server response: %ld \n", bytes_received);
    exit(3);
  }

  printf("Result: %s\n", resp.DebugString().c_str());
}


constexpr short PORT_NUM = 8543;
int main() {
  printf("Starting!\n");
  struct sockaddr_in server;
  server.sin_family = AF_INET;

  in_addr_t address = inet_addr("127.0.0.1");
  if(address == INADDR_NONE)
  {
    perror("Cannot parse IPv4 Address\n");
    exit(2);
  }
  server.sin_addr.s_addr = address;
  server.sin_port = htons(PORT_NUM);

  int server_fd = socket(AF_INET, SOCK_DGRAM, 0);
  if(server_fd < 0)
  {
    fprintf(stderr, "Unable to open server fd!\n");
    exit(5);
  }

  printf("Sending Message!\n");
  SendMessage(&server, server_fd);
  printf("Receiving Response!!\n");
  ReceiveMessage(&server, server_fd);
  printf("Done!\n");
  return 0;
}
