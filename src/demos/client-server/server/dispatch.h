#pragma once

#include "basic_character.pb.h"
#include "server_info.h"

// Forward-Declaration.
void DispatchHandler(ServerInfo& si, const character::Request& client_request, character::Response& resp);
