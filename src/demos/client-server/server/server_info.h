#pragma once
#include <arpa/inet.h>

#include "basic_character.pb.h"

// Client
constexpr size_t MAX_NUM_CONNECTIONS = 64;

typedef int ClientSocket;

struct ClientList {
  ClientSocket clients[MAX_NUM_CONNECTIONS];
  size_t num_clients;
};


typedef int ServerSocket;

struct ServerInfo{
  character::CharacterList client_entities;
  struct sockaddr_in server;
  ServerSocket server_socket;

  ClientList clients;
};

