#include "Renderer.h"

Renderer::Renderer()
{

    InitWindow(640, 480, "mmorp/g/ WIP demo");

    initCamera();

    entities = {};

    hideWindow();
#ifdef DEBUG
    SetTraceLogLevel(LOG_DEBUG);
#endif

}

Renderer::~Renderer() = default;

void Renderer::presentWindow()
{
    ClearWindowState(FLAG_WINDOW_HIDDEN);
}

void Renderer::hideWindow()
{
    SetWindowState(FLAG_WINDOW_HIDDEN);
}

bool Renderer::closeRequested()
{
    return WindowShouldClose();
}

Camera *Renderer::getCurrentCamera()
{
    return &currentCamera;
}

void Renderer::loadLevel(MMO::Level level)
{
    currentLevel = level;
}

void Renderer::addEntityTarget(MMO::EntityDisplay* entityDisplay)
{
    entities.push_back(entityDisplay);
}

void Renderer::update(float dt)
{
    renderTargets();
}

void Renderer::renderTargets()
{
    startRenderpass();
    {
        renderLevel();
        renderEntities();
    }
    endRenderpass();
}

void Renderer::renderLevel()
{
    BeginMode3D(currentCamera);
    {
        //TODO: draw level segments based on position and render distance
        DrawModel(currentLevel.getModel(), {0, 0, 0}, 1.0f, WHITE);
    }
    EndMode3D();
}

void Renderer::renderEntities()
{
    BeginMode3D(currentCamera);
    {
        for(MMO::EntityDisplay* e : entities)
            for(Model m : e->getModels())
                DrawModelEx( m, {(e->x()), (e->y()), (e->z())},
                             {0,0,0}, 0 ,{1.0f, 1.0f, 1.0f}, RED); //TODO implement entity space
    }
    EndMode3D();
}

void Renderer::startRenderpass()
{
    ClearBackground(WHITE); // equivalent to load op clear
    UpdateCamera(&currentCamera);
    DrawFPS(0,0);
    BeginDrawing();
}

void Renderer::endRenderpass()
{
    EndDrawing();
}

void Renderer::initCamera()
{
    currentCamera.position = (Vector3) { 30.0f, 30.0f, 30.0f };
    currentCamera.target = (Vector3) { 0.0f, 0.0f, 0.0f };
    currentCamera.up = (Vector3) { 0.0f, 1.0f, 0.0f };
    currentCamera.fovy = 45.0f;
    currentCamera.projection = CAMERA_CUSTOM;
}