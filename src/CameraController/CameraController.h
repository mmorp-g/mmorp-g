#ifndef MMORP_G_CAMERACONTROLLER_H
#define MMORP_G_CAMERACONTROLLER_H

#include <mmolib/Target.h>

#include <raylib.h>
#include <raymath.h>



class CameraController : public MMO::Target
{

public:

    Camera* camera;

protected:

    Vector2 rightClickStartPos;

};


#endif //MMORP_G_CAMERACONTROLLER_H
