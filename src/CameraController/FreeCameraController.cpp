#include "FreeCameraController.h"

FreeCameraController::FreeCameraController(Camera* camera)
{
    this->speed = 5; //magic speed var TODO: replace with setting
}

FreeCameraController::~FreeCameraController() = default;

void FreeCameraController::update(float dt) {

    if(IsKeyDown(KEY_W))
        camera->position.z += speed * dt;

    if(IsKeyDown(KEY_S))
        camera->position.z -= speed * dt;

    if(IsKeyDown(KEY_A))
        camera->position.x -= speed * dt;

    if(IsKeyDown(KEY_D))
        camera->position.x += speed * dt;

    if(IsKeyDown(KEY_Q))
        camera->position.y -= speed * dt;

    if(IsKeyDown(KEY_E))
        camera->position.y += speed * dt;
}
