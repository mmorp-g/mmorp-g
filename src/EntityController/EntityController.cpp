#include "EntityController.h"

EntityController::EntityController(MMO::Entity& entity, const EntityCameraController& ecc)
: entity(entity)
{
	this->camera = ecc.camera;
	this->collisionModels = {};
	movementVector = {0,0,0};
}

EntityController::~EntityController() = default;

void EntityController::update(float dt)
{
	GM::Vector3f entityPos = {entity.x, entity.y, entity.z};
	GM::Vector3f cameraPos = {camera->position.x, camera->position.y, camera->position.z};

	// track our distance to the camera for a direction
	GM::Vector3f cameraDiff = entityPos.sub( cameraPos );
	GM::Vector3f cameraDiffNorm = cameraDiff.normalise();

	////////////////
	/// movement ///
	////////////////

	if(IsKeyDown(KEY_W))
	{
		movementVector = {
			movementVector.x + ( cameraDiffNorm.x * dt),
			movementVector.y,
			movementVector.z + ( cameraDiffNorm.z * dt)
		};
	}

	if(IsKeyDown(KEY_S))
	{
		movementVector = {
			movementVector.x + ( cameraDiffNorm.x * -dt),
			movementVector.y,
			movementVector.z + ( cameraDiffNorm.z * -dt)
		};
	}

	movementVector = { movementVector.x * ( 1 - ( 5 * dt ) ), movementVector.y, movementVector.z * (1 - ( 5 * dt ) ) };

	///////////////////////
	/// process falling ///
	///////////////////////
	movementVector = {movementVector.x, movementVector.y - (0.07f * dt), movementVector.z};

	/////////////////////////////////
	/// separate xyz col tracking ///
	/////////////////////////////////
	/// a bit overly complex to the point why a bounding box might be better
	/// but it should to super quick to verify sever side
	GM::Vector3f xCheck = GM::Vector3f{movementVector.x, 0, 0}.normalise();
	GM::Vector3f yCheck = GM::Vector3f{0, movementVector.y, 0}.normalise();
	GM::Vector3f zCheck = GM::Vector3f{0, 0, movementVector.z}.normalise();

	Ray xRay = { entity.x, entity.y, entity.z,
	             xCheck.x, xCheck.y, xCheck.z };

	Ray yRay = { entity.x, entity.y, entity.z,
		     yCheck.x, yCheck.y, yCheck.z };

	Ray zRay = { entity.x, entity.y, entity.z,
		     zCheck.x, zCheck.y, zCheck.z };

	float xHitDist = MAXFLOAT;
	float yHitDist = MAXFLOAT;
	float zHitDist = MAXFLOAT;

	for(const Model* m : collisionModels)
	{
		RayHitInfo xHit = GetCollisionRayModel(xRay, *m);
		RayHitInfo yHit = GetCollisionRayModel(yRay, *m);
		RayHitInfo zHit = GetCollisionRayModel(zRay, *m);

		if (xHit.hit && xHit.distance < xHitDist)
		{
			xHitDist = xHit.distance;
		}

		if (yHit.hit && yHit.distance < yHitDist)
		{
			yHitDist = yHit.distance;
		}

		if (zHit.hit && zHit.distance < zHitDist)
		{
			zHitDist = zHit.distance;
		}
	}

	xCheck.mul(xHitDist);
	yCheck.mul(yHitDist);
	zCheck.mul(zHitDist);

	if( std::abs(xHitDist) < std::abs(movementVector.x) )
	{
		movementVector = {zHitDist, movementVector.y, movementVector.z};
	}

	if( std::abs(yHitDist) < std::abs(movementVector.y) )
	{
		movementVector = {movementVector.x, yHitDist, movementVector.z};
	}

	if( std::abs(zHitDist) < std::abs(movementVector.z))
	{
		movementVector = {movementVector.x, movementVector.y, zHitDist};
	}

	//////////////////////////////
	/// update entity position ///
	//////////////////////////////
	if(std::abs(movementVector.x) > 0.01)
	{
		entity.x += movementVector.x;
	}

	if(std::abs(movementVector.y) > 0.01)
	{
		entity.y += movementVector.y;
	}

	if(std::abs(movementVector.z) > 0.01)
	{
		entity.z += movementVector.z;
	}
}

void EntityController::addCollisionModel(const Model* model)
{
	collisionModels.push_back(model);
}