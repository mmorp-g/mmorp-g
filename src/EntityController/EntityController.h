#ifndef MMORP_G_ENTITYCONTROLLER_H
#define MMORP_G_ENTITYCONTROLLER_H

#include "../CameraController/EntityCameraController.h"

#include <vector>
#include <mmolib/Target.h>
#include <gMath/Vector2f.h>
#include <gMath/Vector3f.h>
#include <cstdio>

class EntityController : public MMO::Target
{

public:

	EntityController(MMO::Entity &entity, const EntityCameraController &camera);

	virtual ~EntityController();

	virtual void update(float dt) override;

	void addCollisionModel(const Model *model);

protected:

	const Camera *camera;

	MMO::Entity &entity;

	std::vector<const Model *> collisionModels;

	GM::Vector3f movementVector;
};

#endif //MMORP_G_ENTITYCONTROLLER_H