import os
import json
import asyncio

import datetime
from datetime import datetime as dt, timezone

import dateutil.parser
from dateutil.relativedelta import relativedelta

import discord # type: ignore
from dotenv import load_dotenv

import requests

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

class MergeRequest:
    def __init__(self, mr_map):
        self.LAST_WEEK = dt.now(timezone.utc) - relativedelta(weeks=+1)

        # Use internal-ID, as that's what's displayed in the online platform.
        self.mr_id = mr_map["iid"]
        self.title = mr_map["title"]
        self.state = mr_map["state"]

        # Timestamps are in ISO 8601 format. (https://docs.gitlab.com/ee/api/merge_requests.html)
        self.creation_time = dateutil.parser.isoparse(mr_map["created_at"])

        self.resolve_time = None
        if mr_map["merged_at"] is not None:
            self.resolve_time = dateutil.parser.isoparse(mr_map["merged_at"])

        self.url = mr_map["web_url"]

    def merged_in_last_week(self):
        """Returns TRUE if this MR was closed within the last week"""
        if self.state == "opened":
            return False
        return self.resolve_time > self.LAST_WEEK

    def created_in_last_week(self):
        """Returns TRUE if this MR was opened within the last week"""
        return self.creation_time > self.LAST_WEEK

    def __str__(self):
        if self.state == "opened":
            return f"#{self.mr_id} - {self.title} | {self.url}"
        else:
            return f"#{self.mr_id} - {self.title}"

class RepoIssue:
    def __init__(self, issue_dict):
        labels = issue_dict["labels"]
        self.in_progress = False
        if "In progress" in labels:
            self.in_progress = True

        self.closed = False
        self.closed_at = issue_dict["closed_at"]
        if self.closed_at is not None:
            self.closed = True

        self.issue_id = issue_dict["iid"]
        self.issue_link = issue_dict["_links"]["self"]
        self.title = issue_dict["title"]

        self.LAST_WEEK = dt.now(timezone.utc) - relativedelta(weeks=+1)

    def is_in_progress(self):
        return self.in_progress

    def is_closed(self):
        return self.closed

    def recently_closed(self):
        return self.closed_at > self.LAST_WEEK

    def __str__(self):
        if self.in_progress:
            return f"#{self.issue_id}: {self.title} | {self.issue_link}"
        elif self.is_closed:
            return f"#{self.issue_id}: {self.title}"
        else:
            return "<>"


class RepoInfoQuerier:
    """Given a gitlab repo, provides functionality to retrieve information"""
    def __init__(self):
        self.GITLAB_TOKEN=os.getenv('GITLAB_TOKEN')
        self.MMORPG_PROJECT_ID = 27975757
        self.MMORPG_DEV_BOARD_ID = 2915969

        self.issues_link = None
        self.mr_link = None
        self.__get_project_links()

        self.open_mrs = []
        self.merged_mrs = []
        self.__analyze_merge_requests()

        self.issues = []
        self.__get_issues()


    def __make_request(self, url):
        print(f"URL: {url}")
        headers = self.__create_headers()
        r = requests.get(url, headers=headers)
        if r.status_code != 200:
            raise ValueError(f"Unsuccesful request to project: {r}")

        response_json = r.json()
        return response_json

    def __get_project_links(self):
        """Queries the project resource to get links to merge requests and the
issues."""
        url = self.__create_project_url(self.MMORPG_PROJECT_ID)
        response_json = self.__make_request(url)

        links = response_json["_links"]
        self.issues_link = links["issues"]
        self.mr_link = links["merge_requests"]

    def __analyze_merge_requests(self):
        url = self.__create_mr_url("opened")
        response_json = self.__make_request(url)

        for mr in response_json:
            self.open_mrs += [MergeRequest(mr)]

        url = self.__create_mr_url("merged")
        response_json = self.__make_request(url)

        for mr in response_json:
            self.merged_mrs += [MergeRequest(mr)]

    def get_new_open_merge_requests(self):
        """Get Open MRs created less than a week ago"""
        return list(filter(lambda mr: mr.created_in_last_week(),
                           self.open_mrs))

    def get_stale_merge_requests(self):
        """Get Open MRs created less than a week ago"""
        return list(filter(lambda mr: not mr.created_in_last_week(),
                           self.open_mrs))

    def get_recently_merged_merge_requests(self):
        """Get MRs merged within the last week."""
        return list(filter(lambda mr: mr.merged_in_last_week(),
                           self.merged_mrs))


    def get_done_merge_requests(self):
        url = self.__create_mr_url("merged")
        response_json = self.__make_request(url)

        return response_json

    def __create_headers(self):
        return {
            "content-type": "application/json",
            "PRIVATE-TOKEN": str(self.GITLAB_TOKEN)
        }

    def __create_project_url(self, value):
        return f"https://gitlab.com/api/v4/projects/{str(value)}"

    def __create_mr_url(self, state):
        return f"{self.mr_link}?state={state}"

    def __get_issues(self):
        url = f"{self.__create_project_url(self.MMORPG_PROJECT_ID)}/issues?state=opened"
        for issue in self.__make_request(url):
            self.issues += [RepoIssue(issue)]

    def get_recently_closed_issues(self):
        result = list(filter(lambda issue: issue.is_closed() and issue.recently_closed(),
                           self.issues))
        # Return top 5 issues
        return result[0:4]

    def get_in_progress_issues(self):
        return list(filter(lambda issue: issue.is_in_progress(),
                           self.issues))

    def get_repo_summary(self):
        mr_info = [
            ("OPEN", self.get_new_open_merge_requests()),
            ("STALE", self.get_stale_merge_requests()),
            ("RECENTLY CLOSED", self.get_recently_merged_merge_requests()),
        ]

        mr_info_present = False
        summary = ""

        mr_summary = ""
        for label, mrs in mr_info:
            if len(mrs) == 0:
                continue
            mr_info_present = True
            mr_str = "\n".join(map(lambda mr: str(mr), mrs))
            mr_summary += f"### _{label}_ ###\n{mr_str}\n\n"

        if mr_info_present:
            mr_summary = "# *MR INFO* #\n" + mr_summary

        issue_info = [
            ("RECENTLY CLOSED", self.get_recently_closed_issues()),
            ("IN PROGRESS", self.get_in_progress_issues()),
        ]
        issue_info_present = False
        issue_summary = ""
        for label, issues in issue_info:
            if len(issues) == 0:
                continue

            print("Length of issues: " + str(len(issues)))

            issue_info_present = True
            issue_str = "\n".join(map(lambda issue: str(issue), issues))
            issue_summary += f"### _{label}_ ###\n{issue_str}\n"


        if issue_info_present:
            issue_summary = "# *ISSUE INFO* #\n" + issue_summary

        return f"{mr_summary}\n{issue_summary}"


class NonInteractiveClient(discord.Client):
    async def on_ready(self):
        await self.wait_until_ready()
        print(f'{client.user} has connected to Discord!')
        channel_id = 876736399469076490
        channel = client.get_channel(channel_id)

        query = RepoInfoQuerier()
        summary = query.get_repo_summary()

        prelude = "Hello Master. I have information to report "

        epilogue = "I calculate a 24% chance of getting something done in the near future."

        await channel.send(prelude + "\n" + summary + "\n" + epilogue)

        await self.close()


client = NonInteractiveClient()
client.run(TOKEN)
print("Done")
